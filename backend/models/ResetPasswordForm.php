<?php
namespace backend\models;

use common\models\Student;
use yii\base\Model;

/**
 * Random password reset request form
 */
class ResetPasswordForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\Student',
//                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email random password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user Student */
        $user = Student::findOne([
            'status' => Student::STATUS_ACTIVE,
            'email' => $this->email,
        ]);
        /* if user existe */
        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($this->email)
                    ->setSubject('Password for you: ' . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
