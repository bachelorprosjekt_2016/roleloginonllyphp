<!--Users login-->


    <?php
    /* @var $this yii\web\View */
    /* @var $form yii\bootstrap\ActiveForm */
    /* @var $model \common\models\LoginForm */

    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

$this->title = 'Bruker Logg inn';
//    $this->params['breadcrumbs'][] = $this->title;
    ?>
    
    <div id="content">
    <div id="title" class="desktop-only"><h1>DATA REGISTER - BJØRNHOLT SKOLE</h1></div>

    
<div class="row main-content">  
    <div class="col-md-6 col-md-offset-3 login-wrapper lightblue-bg box-shadow login_box">
        
        <div class="padding-top-20">
        <div class="image-logo">   
        <img src="../../../web/assets/images/user.png" 
             class="img-circle box-shadow" 
             style= "height:60px;width:60px"/></div>
    </div>
        
    <div class="site-login">
        <div class="info"><h1><?= Html::encode($this->title) ?></h1></div>

        <div class="info"><p>Skriv inn følgende for å logge inn:</p></div>
        
       
        <div class="row">
            <div class="col-md-12">
                
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                
                <?=
                    $form->field($model, 'username')
                    ->label('Fødselsnummer')
                ?>

                <?=
                    $form->field($model, 'password')
                    ->label('Passord')
                    ->passwordInput()
                ?>

                <!--                <div style="color:#999;margin:1em 0">
                                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                                </div>-->
                <div class="pull-left">
                <a href="#">Glemt passord?</a>
                </div>
                
                <div class="form-group pull-right">
<?= Html::submitButton('Logg inn', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    </div>
</div>
</div>